//
//  AccessoryThermostatViewController.m
//  FancyControl
//
//  Created by Tony on 2014/7/22.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import "AccessoryThermostatViewController.h"

@interface AccessoryThermostatViewController ()

@end

@implementation AccessoryThermostatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.accessory.delegate = self;
    self.title = self.accessory.name;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    for (HMService *service in self.accessory.services) {
        
        if ([service.serviceType isEqualToString:HMServiceTypeAccessoryInformation]) {
            for (HMCharacteristic *charac in service.characteristics) {
                //                NSLog(@"Characteristics : %@ in %@",charac.value,service.name);
                if ([charac.characteristicType isEqualToString:HMCharacteristicTypeName]) {
                    self.accessoryName.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeManufacturer]){
                    self.manufacturer.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeModel]){
                    self.model.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeSerialNumber]){
                    self.serialNo.text = charac.value;
                }
            }
            
        }else if ([service.serviceType isEqualToString:HMServiceTypeThermostat]){
            for (HMCharacteristic *charac in service.characteristics) {
                NSLog(@"%@ , %d",charac.characteristicType,charac.notificationEnabled);
                if ([charac.characteristicType isEqualToString:HMCharacteristicTypeTargetTemperature]) {
                    self.targetTemp.text = [NSString stringWithFormat:@"%@",charac.value];
                    self.tempSteper.value = [charac.value floatValue];
                    targetCharacteristic = charac;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeCurrentTemperature]){
                    self.currentTemp.text = [NSString stringWithFormat:@"%@",charac.value];
                    [charac enableNotification:YES completionHandler:^(NSError *error) {
                        if (error != nil) {
                        }else{
                            NSLog(@"Notify current temp enable!");
                        }
                    }];
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeTargetHeatingCooling]){
                    NSLog(@"Target mode %@",charac.value);
                    targetModeCharacteristic = charac;
//                    if ([charac.value isEqualToString:@"off"]) {
//                        self.targetModeSeg.selectedSegmentIndex = 0;
//                    }else if([charac.value isEqualToString:@"heat"]){
//                        self.targetModeSeg.selectedSegmentIndex = 1;
//                    }else if([charac.value isEqualToString:@"cool"]){
//                        self.targetModeSeg.selectedSegmentIndex = 2;
//                    }else{
//                        self.targetModeSeg.selectedSegmentIndex = 3;
//                    }
                    self.targetModeSeg.selectedSegmentIndex = [charac.value intValue];
                    
                }
            }
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -delegate
-(void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic{
    self.currentTemp.text = [NSString stringWithFormat:@"%@",characteristic.value];
}
#pragma mark - action
- (IBAction)changeTempBtn:(id)sender {
    UIStepper *steper = (UIStepper*) sender;
    self.targetTemp.text = [NSString stringWithFormat:@"%.0f",steper.value];
    
    [targetCharacteristic writeValue:[NSNumber numberWithFloat:steper.value] completionHandler:^(NSError *error) {
        if (error!=nil) {
            
        }else{
            NSLog(@"Change temp success!");
        }
    }];
}

- (IBAction)changeModeBtn:(id)sender {
    UISegmentedControl *segment = (UISegmentedControl*)sender;
    NSNumber *mode = [NSNumber numberWithInt:3];
    switch (segment.selectedSegmentIndex) {
        case 0:
            mode = [NSNumber numberWithInt:0];
            break;
        case 1:
            mode = [NSNumber numberWithInt:1];
            break;
        case 2:
            mode = [NSNumber numberWithInt:2];
            break;
            
        default:
            break;
    }
    [targetModeCharacteristic writeValue:mode completionHandler:^(NSError *error) {
        if (error != nil) {
            
        }else{
            NSLog(@"Change mode success!");
        }
    }];
}
@end
