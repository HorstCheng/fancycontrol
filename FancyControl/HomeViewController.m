//
//  HomeViewController.m
//  FancyControl
//
//  Created by Tony on 2014/7/16.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import "HomeViewController.h"
#import "AccessoryViewController.h"

@interface HomeViewController ()
{
    UITextField *textField;
    HMAccessory *foundAcc;
    HMAccessory *currentAcc;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    newAccessorys = [[NSMutableArray alloc]initWithCapacity:0];
    
//    setup accessory browser
    self.browser = [[HMAccessoryBrowser alloc]init];
    self.browser.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    rooms = [NSArray arrayWithArray:self.home.rooms];
    self.title = self.home.name;
    
    
    
//    for (HMAccessory *acc in self.home.accessories) {
//        [self.home removeAccessory:acc completionHandler:^(NSError *error) {
//            if (error !=nil) {
//                
//            }else{
//                NSLog(@"remove accessory : %@",acc.name);
//            }
//        }];
//    }
    
    accessorys = self.home.accessories;
    
    [self.tbl reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return rooms.count+2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return newAccessorys.count;
    }else if (section==1){
        if (accessorys==nil) {
            return 0;
        }else{
            return accessorys.count;
        }
    }else{
        return [(HMRoom*)[rooms objectAtIndex:section-2] accessories].count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"RoomCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
//    cell.accessoryType = UITableViewCellAccessoryNone;
    HMAccessory *acc;
    if (indexPath.section==0) {
        acc = [self.browser.discoveredAccessories objectAtIndex:indexPath.row];
    }else if (indexPath.section==1) {
        acc = [accessorys objectAtIndex:indexPath.row];
    }else{
        HMRoom *room = [rooms objectAtIndex:indexPath.section-2];
        acc = [room.accessories objectAtIndex:indexPath.row];
        
    }
    
    cell.textLabel.text = acc.name;
    
    return cell;



}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
        HMAccessory *newAccessory = [newAccessorys objectAtIndex:indexPath.row];
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Add Accessory %@",newAccessory.name] message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//        
//        //    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
//        alert.tag = 200;
//        [self.browser stopSearchingForNewAccessories];
//        [alert show];
        [self.home addAccessory:newAccessory completionHandler:^(NSError *error) {
            if (error != nil) {
                NSLog(@"%@ :",error.debugDescription);
            }else{
                NSLog(@"%@ accessory successfully added",newAccessory.name);
                accessorys = self.home.accessories;
                
                [self.tbl reloadData];
            }
        }];
        

    }else if (indexPath.section==1) {
        currentAcc = [self.home.accessories objectAtIndex:indexPath.row];
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add to Room"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil,
                                      nil];
        
        
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        actionSheet.tag = 200;
        
        for (HMRoom *room in rooms) {
            [actionSheet addButtonWithTitle:room.name];
        }
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // In this case the device is an iPad.
            [actionSheet showFromRect:[tableView cellForRowAtIndexPath:indexPath].frame inView:self.view animated:YES];
        }
        else{
            // In this case the device is an iPhone/iPod Touch.
            [actionSheet showInView:self.view];
        }

    }else{
        HMRoom *room = [self.home.rooms objectAtIndex:indexPath.section-2];
        HMAccessory *accessory = [room.accessories objectAtIndex:indexPath.row];
        if ([accessory.services count]>1) {
            HMService *service = [accessory.services objectAtIndex:1];
            if ([service.serviceType isEqualToString:HMServiceTypeLightbulb]) {
                [self performSegueWithIdentifier:@"showLightBulbAccessory" sender:self];
            }else{
                [self performSegueWithIdentifier:@"showThermostatAccessory" sender:self];
            }

        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please add some service!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tbl indexPathForSelectedRow];
    
    AccessoryViewController *vc = [segue destinationViewController];
    
    HMRoom *selectedRoom = [rooms objectAtIndex:indexPath.section-2];
    vc.accessory = [selectedRoom.accessories objectAtIndex:indexPath.row];
    
}



- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"More" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        [self.tbl setEditing:NO];
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *blurAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Blur" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.tbl setEditing:NO];
    }];
    blurAction.backgroundEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        
        
        if (indexPath.section>0) {
            [self.home removeAccessory:[self.home.accessories objectAtIndex:indexPath.row]  completionHandler:^(NSError *error) {
                if (error!=nil) {

                }else{
                    
                    accessorys = self.home.accessories;
                    [self.tbl reloadData];
                }
            }];
        }

    }];
    
    return @[deleteAction];
}

//// From Master/Detail Xcode template
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [self.objects removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.section>0) {
            [self.home removeAccessory:[self.home.accessories objectAtIndex:indexPath.row]  completionHandler:^(NSError *error) {
                if (error!=nil) {
                    
                }else{
                    
                    NSLog(@"remove accessory success!");
                    accessorys = self.home.accessories;
                    
                    [self.tbl reloadData];
                }
            }];
        }
        
    }
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return @"Found Accessory";
    }else if (section==1) {
        return [NSString stringWithFormat:@"Accessory @[%@]",self.home.name];
    }else{
        return [NSString stringWithFormat:@"%@ @[%@]",[(HMRoom*)[rooms objectAtIndex:section-2] name],self.home.name];
    }
}
#pragma mark - delegate 
-(void)accessoryBrowser:(HMAccessoryBrowser *)browser didRemoveNewAccessory:(HMAccessory *)accessory{
    newAccessorys = self.browser.discoveredAccessories;
    [self.tbl reloadData];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==100) {

        if (buttonIndex == 1) {
            UITextField *inputStr = [alertView textFieldAtIndex:0];
            [self.home addRoomWithName:inputStr.text completionHandler:^(HMRoom
                                                                                 *room, NSError *error) {
                if (error != nil) {
                    // unable to add room. check error for why
                    NSLog(@"%@",error);
                } else {
                    // success!
                    NSLog(@"Add Room success");
                    rooms = self.home.rooms;
                    [self.tbl reloadData];
                } }];

        }
    }else if (alertView.tag==200){
        [self.browser stopSearchingForNewAccessories];
        
        if (buttonIndex == 0) {
            //取消
            [self.browser startSearchingForNewAccessories];
        }else{
            [self.home addAccessory:foundAcc completionHandler:^(NSError *error) {
                if (error != nil) {
                    NSLog(@"%@ :",error.debugDescription);
                }else{
                    NSLog(@"%@ accessory successfully added",foundAcc.name);
                    accessorys = self.home.accessories;
                    [self.tbl reloadData];
                }
            }];
            
        }
    }
}

-(void)accessoryBrowser:(HMAccessoryBrowser *)browser didFindNewAccessory:(HMAccessory *)accessory{
    NSLog(@"Found Accessory : %@",accessory.name);
//    foundAcc = accessory;
    
//    if (![newAccessorys containsObject:accessory]) {
//        [newAccessorys addObject:accessory];
//    }
//
    newAccessorys = self.browser.discoveredAccessories;
    [self.tbl reloadData];
    
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag==100) {
        if (buttonIndex==0) {
            [self searchAccessorys];
        }else if(buttonIndex==1){
            [self addRoom];
        }
    }else{
        if (buttonIndex > 0) {
            [self.home assignAccessory:currentAcc toRoom:[self.home.rooms objectAtIndex:buttonIndex-1] completionHandler:^(NSError *error) {
                if (!error) {
                    NSLog(@"Accessory successfully assigned to room");
                    [self.tbl reloadData];
                }
            }];

        }
    }
    
}


#pragma mark - actions

- (IBAction)addRoom:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:
                                  @"Search Accessorys",
                                  @"Add Room",
                                  nil];
//    actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
    actionSheet.tag = 100;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // In this case the device is an iPad.
        [actionSheet showFromBarButtonItem:sender animated:YES];
    }
    else{
        // In this case the device is an iPhone/iPod Touch.
        [actionSheet showInView:self.view];
    }
        
//    UIActionSheetCon

    
}

-(void) searchAccessorys{
    [self.browser startSearchingForNewAccessories];
}

-(void) addRoom{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Room Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];

    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    alert.tag = 100;
    [alert show];
}
@end
