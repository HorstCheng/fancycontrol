//
//  AppDelegate.h
//  FancyControl
//
//  Created by Tony on 2014/7/15.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

