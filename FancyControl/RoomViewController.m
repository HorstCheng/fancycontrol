//
//  RoomViewController.m
//  FancyControl
//
//  Created by Tony on 2014/7/16.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import "RoomViewController.h"

@interface RoomViewController ()
{
    NSArray *accessorys;
    HMAccessory *foundAcc;
}

@end

@implementation RoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    accessorys = self.room.accessories;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return accessorys.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"AccessoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    HMAccessory *accessory = [accessorys objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    cell.textLabel.text = accessory.name;
    
    return cell;

}


#pragma mark - delegate


#pragma mark - action

- (IBAction)findAccessory:(id)sender {

}
@end
