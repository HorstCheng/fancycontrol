//
//  main.m
//  FancyControl
//
//  Created by Tony on 2014/7/15.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
