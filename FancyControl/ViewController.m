//
//  ViewController.m
//  FancyControl
//
//  Created by Tony on 2014/7/15.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"


@interface ViewController()
{
    HMAccessory *foundAcc;
}

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.homeManager = [[HMHomeManager alloc] init];
    self.homeManager.delegate = self;
    
    
//    __weak typeof(self) weakSelf = self;
//    
//    [self.homeManager addHomeWithName:@"My Home"
//                    completionHandler:^(HMHome *home, NSError *error) {
//                        if (error != nil) {
//                            // adding the home failed; check error for why
//                            NSLog(@"%@",error);
//                        } else {
//                            // success!
//                            NSLog(@"Created Home : %@",home.name);
//                            weakSelf.home = home;
//                            
//                        }
//                    }];
    
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.homes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"HomeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    HMHome *home = [self.homes objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    cell.textLabel.text = home.name;
    
    return cell;
    
    

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   [self performSegueWithIdentifier: @"detail" sender: self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tbl indexPathForCell:sender];
    
    HomeViewController *vc = [segue destinationViewController];
    
    vc.home = [self.homes objectAtIndex:indexPath.row];
    
}



- (void)accessoryDidUpdateReachability:(HMAccessory *)accessory {
    if (accessory.reachable == YES) {
        // we can communicate with the accessory

    } else {
        // the accessory is out of range, turned off, etc
    }
}


-(void)homeManager:(HMHomeManager *)manager didAddHome:(HMHome *)home{
    [self.tbl reloadData];
}
-(void)homeManager:(HMHomeManager *)manager didRemoveHome:(HMHome *)home{
    NSLog(@"Delete Home %@",home.name);
}

-(void)homeManagerDidUpdateHomes:(HMHomeManager *)manager{
    self.homes = manager.homes;
    if (manager.homes.count == 0) {
        __weak typeof(self) weakSelf = self;
        
        [self.homeManager addHomeWithName:@"My Home"
                        completionHandler:^(HMHome *home, NSError *error) {
                            if (error != nil) {
                                // adding the home failed; check error for why
                                NSLog(@"%@,xxx%ld",error.debugDescription,(long)HMErrorCodeHomeAccessNotAuthorized);
                                
                            } else {
                                // success!
                                NSLog(@"Created Home : %@",home.name);
                                [weakSelf.tbl reloadData];
                            }
                        }];

    }else{
        self.home = [manager.homes objectAtIndex:0];
//        [self.homeManager removeHome:self.home completionHandler:^(NSError *error) {
//            
//        }];
        [self.tbl reloadData];
    }

    
    
    
    
//    NSString *homeName = self.home.name;
//    NSArray *allRooms = self.home.rooms;
//    NSArray *allAccessories = self.home.accessories;
    
    
//    [self.home addRoomWithName:@"Command Center" completionHandler:^(HMRoom
//                                                                         *room, NSError *error) {
//        if (error != nil) {
//            // unable to add room. check error for why
//            NSLog(@"%@",error);
//        } else {
//            // success!
//            NSLog(@"Add Room success");
//        } }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchBtn:(id)sender {
    
//    self.browser = [[HMAccessoryBrowser alloc]init];
//    self.browser.delegate = self;
//    [self.browser startSearchingForNewAccessories];

}


#pragma mark - delegate

@end
