//
//  AccessoryThermostatViewController.h
//  FancyControl
//
//  Created by Tony on 2014/7/22.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface AccessoryThermostatViewController : UIViewController<HMAccessoryDelegate>
{
    HMCharacteristic *targetCharacteristic;
    HMCharacteristic *targetModeCharacteristic;
}

@property (weak, nonatomic) HMAccessory *accessory;

@property (weak, nonatomic) IBOutlet UILabel *accessoryName;
@property (weak, nonatomic) IBOutlet UILabel *manufacturer;
@property (weak, nonatomic) IBOutlet UILabel *model;
@property (weak, nonatomic) IBOutlet UILabel *serialNo;

@property (weak, nonatomic) IBOutlet UILabel *targetTemp;
@property (weak, nonatomic) IBOutlet UILabel *currentTemp;
@property (weak, nonatomic) IBOutlet UIStepper *tempSteper;
@property (weak, nonatomic) IBOutlet UISegmentedControl *targetModeSeg;
@property (weak, nonatomic) IBOutlet UISegmentedControl *changeMode;

- (IBAction)changeTempBtn:(id)sender;

- (IBAction)changeModeBtn:(id)sender;


@end
