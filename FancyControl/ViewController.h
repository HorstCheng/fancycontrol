//
//  ViewController.h
//  FancyControl
//
//  Created by Tony on 2014/7/15.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;


@interface ViewController : UIViewController<HMHomeManagerDelegate,UITableViewDataSource,UITableViewDelegate,HMAccessoryDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tbl;

@property (nonatomic,strong) HMHomeManager *homeManager;

@property (nonatomic,strong) NSArray *homes;

@property (nonatomic,strong) HMHome *home;




- (IBAction)searchBtn:(id)sender;

@end

