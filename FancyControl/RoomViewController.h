//
//  RoomViewController.h
//  FancyControl
//
//  Created by Tony on 2014/7/16.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface RoomViewController : UIViewController<HMAccessoryBrowserDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tbl;

@property (nonatomic,weak) HMRoom *room;

@property (nonatomic,strong) HMAccessoryBrowser *browser;
- (IBAction)findAccessory:(id)sender;

@end
