//
//  HomeViewController.h
//  FancyControl
//
//  Created by Tony on 2014/7/16.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,HMAccessoryBrowserDelegate,UIActionSheetDelegate>{
    NSArray *rooms;
    NSArray *accessorys;
    NSArray *newAccessorys;
}

@property (nonatomic,weak) HMHome *home;
@property (nonatomic,strong) HMAccessoryBrowser *browser;


@property (weak, nonatomic) IBOutlet UITableView *tbl;


- (IBAction)addRoom:(id)sender;

@end
