//
//  AccessoryViewController.m
//  FancyControl
//
//  Created by Tony on 2014/7/21.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import "AccessoryViewController.h"

@interface AccessoryViewController ()

@end

@implementation AccessoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.accessory.delegate = self;
    
    self.title = self.accessory.name;
    
    [self.powerStte addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self.isSync setOn:NO];
    
    for (HMService *service in self.accessory.services) {
        NSLog(@"Service name is : %@",service.name);
        if ([service.serviceType isEqualToString:HMServiceTypeAccessoryInformation]) {
            for (HMCharacteristic *charac in service.characteristics) {
                NSLog(@"Characteristics : %@ in %@",charac.value,service.name);
                if ([charac.characteristicType isEqualToString:HMCharacteristicTypeName]) {
                    self.accessoryName.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeManufacturer]){
                    self.manufacturerName.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeModel]){
                    self.model.text = charac.value;
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeSerialNumber]){
                    self.serialNo.text = charac.value;
                }
                
            }
        }else if ([service.serviceType isEqualToString:HMServiceTypeLightbulb]){
            for (HMCharacteristic *charac in service.characteristics) {
                if ([charac.characteristicType isEqualToString:HMCharacteristicTypePowerState]) {
                    [charac readValueWithCompletionHandler:^(NSError *error) {
                        if (error==nil) {
                            lightBulbPower = charac;
                            [self.powerStte setOn:[charac.value integerValue]==1?YES:NO];
                        }else{
                            [self.powerStte setEnabled:NO];
                        }
                    }];
                }else if ([charac.characteristicType isEqualToString:HMCharacteristicTypeHue]){
                    [charac readValueWithCompletionHandler:^(NSError *error) {
                        if (error==nil) {
                            hueValue = charac;
                            self.hueSlider.value = [charac.value floatValue];
                            self.hueVal.text = [NSString stringWithFormat:@"%@",charac.value];
                        }else{
                            [self.hueSlider setEnabled:NO];
                        }
                    }];
                }
            }
        }
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - delegate
-(void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic{
    
    
    
    if ([characteristic.characteristicType isEqualToString:HMCharacteristicTypePowerState]) {
        [self.powerStte setOn:[characteristic.value integerValue]==1?YES:NO];
    }else if([characteristic.characteristicType isEqualToString:HMCharacteristicTypeHue]){
        self.hueSlider.value = [characteristic.value intValue];
        self.hueVal.text = [NSString stringWithFormat:@"%@",characteristic.value];
    }
    NSLog(@"service : %@ , characteristic : %@",service.name,characteristic.characteristicType);
}

#pragma mark - action
-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    [lightBulbPower writeValue:[NSNumber numberWithInt:isButtonOn==YES?1:0] completionHandler:^(NSError *error) {
        if (error != nil) {
            // unable to write value. check error for why
        }else{
            NSLog(@"success");
        }
    }];
}

- (IBAction)hueSliderChange:(id)sender {
    self.hueVal.text = [NSString stringWithFormat:@"%.f",self.hueSlider.value];
    [hueValue writeValue:[NSNumber numberWithInt:self.hueSlider.value]  completionHandler:^(NSError *error) {
        if (error!=nil) {
            
        }else{
            NSLog(@"Hue success!");
        }
    }];
}

- (IBAction)syncSwitch:(id)sender {
    [lightBulbPower enableNotification:[self.isSync isOn] completionHandler:^(NSError *error) {
        if (error !=nil) {
            
        }else{
            NSLog(@"lightBuld sync status is : %d",[self.isSync isOn]);
        }
    }];
    
    [hueValue enableNotification:[self.isSync isOn] completionHandler:^(NSError *error) {
        if (error != nil) {
    
        }else{
            NSLog(@"Hue sync status is : %d",[self.isSync isOn]);
        }
    }];
    
    
}
@end
