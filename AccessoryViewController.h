//
//  AccessoryViewController.h
//  FancyControl
//
//  Created by Tony on 2014/7/21.
//  Copyright (c) 2014年 Chainmeans Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface AccessoryViewController : UIViewController<HMAccessoryDelegate>
{
    HMCharacteristic *lightBulbPower;
    HMCharacteristic *hueValue;
}

@property (nonatomic,weak) HMAccessory *accessory;


@property (weak, nonatomic) IBOutlet UILabel *accessoryName;
@property (weak, nonatomic) IBOutlet UILabel *manufacturerName;
@property (weak, nonatomic) IBOutlet UILabel *model;
@property (weak, nonatomic) IBOutlet UILabel *serialNo;
@property (weak, nonatomic) IBOutlet UISwitch *powerStte;
@property (weak, nonatomic) IBOutlet UILabel *bulbState;
@property (weak, nonatomic) IBOutlet UISlider *hueSlider;
- (IBAction)hueSliderChange:(id)sender;
- (IBAction)syncSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *hueVal;

@property (weak, nonatomic) IBOutlet UISwitch *isSync;
@end
